var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');
var ObjectId = mongodb.ObjectId;
var MongoClient = mongodb.MongoClient;

function errorHandler(res) {
  res.status(500).send({message: "Can't connect to Database, try later"});
}

router.get('/api/contacts.json', function(req, res) {
  MongoClient.connect('mongodb://localhost:27017/test', function(err, db) {
    if (err) {
      errorHandler(res);
    } else {
      db.collection('contacts').find().toArray().then(function(contacts) {
        if (!contacts.length) {
          res.status(500).send({message: "You haven't contacts"});
        } else {
          res.send(contacts);
        }
        db.close();
      });
    }
  });
});

router.get('/api/contact/:id', function(req, res) {
  MongoClient.connect('mongodb://localhost:27017/test', function(err, db) {
    if (err) {
      errorHandler(res);
    } else {
      db.collection('contacts').find().toArray()
      .then(function(contacts) {
        // normalize id (front-end start count from 1)
        let id = req.params.id - 1;

        if (id < 0 || id > contacts.length - 1) {
          res.status(404).send({message: 'Contact not found'});
        } else {
          res.send(contacts[id]);
        }
        db.close();
      });
    }
  });
});

router.put('/api/save-contact', function(req, res) {
  MongoClient.connect('mongodb://localhost:27017/test', function(err, db) {
    if (err) {
      errorHandler(res);
    } else {
      let contact = req.body;

      db.collection('contacts').updateOne(
      {
        _id: new ObjectId(contact._id)
      },
      {
        $set: {
          firstName: contact.firstName,
          lastName: contact.lastName,
          phone: contact.phone,
          email: contact.email
        }
      }
      ).then(function() {
        res.send({message: 'Contact saved'});
        db.close();
      }).catch(function(err) {
        res.status(500).send({message: "Can't save contact, try later"});
      });
    }
  });
});

router.put('/api/add-contact', function(req, res) {
  MongoClient.connect('mongodb://localhost:27017/test', function(err, db) {
    if (err) {
      errorHandler(res);
    } else {
      db.collection('contacts').insertOne(req.body).then(function() {
        res.status(201).send({message: 'Contact successfully added'});
        db.close();
      }).catch(function(err) {
        res.status(500).send({message: "Can't add contact, try later"});
      });
    }
  });
});

router.delete('/api/delete-contact', function(req, res) {
  MongoClient.connect('mongodb://localhost:27017/test', function(err, db) {
    if (err) {
      errorHandler(res);
    } else {
      let id = req.body.id;
      db.collection('contacts').deleteOne({_id: new ObjectId(id)})
      .then(function() {
        res.send({message: "Contact successfully deleted"});
        db.close();
      })
      .catch(function(err) {
        res.status(500).send({message: "Can't delete this contact now, try later"});
      });
    }
  });
});


router.get('/api/reset-db', function(req, res, next) {
  MongoClient.connect('mongodb://localhost:27017/test', function(err, db) {
    if (err) {
      errorHandler(res);
    } else {
      db.createCollection('contacts').then(function(collection) {
        collection.drop()
        .then(function(result) {
          db.createCollection('contacts')
          .then(function(collection) {
            collection.insertMany(require('../../config/contacts'))
            .then(function() {
              res.send('DB successfully restored');
              db.close();
            }); 
          });
        });
      });
    }
  });
});

module.exports = router;
