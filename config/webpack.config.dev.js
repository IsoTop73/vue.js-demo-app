var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    main: [
      require.resolve('webpack-dev-server/client') + '?/',
      require.resolve('webpack/hot/dev-server')
    ]
  },

  devtool: '#eval-source-map',

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"development"'
      }
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve('./frontend/index.html'),
      inject: true
    })
  ],

  devServer: {
    clientLogLevel: 'none',
    contentBase: './frontend',
    hot: true,
    publicPath: '/',
    proxy: [{
      path: '/api/',
      target: 'http://localhost:3000'
    }],
    watchOptions: {
      ignored: /node_modules/
    }
  }
};