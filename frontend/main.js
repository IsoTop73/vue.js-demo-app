import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'
import About from './pages/About'
import Contact from './pages/Contact'
import Home from './pages/Home'
import Add from './pages/Add'
import Edit from './pages/Edit'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/about',
      component: About
    },
    {
      path: '/contact/:id',
      component: Contact
    },
    {
      path: '/contact/:id/edit',
      component: Edit
    },
    {
      path: '/add-contact',
      component: Add
    }
  ]
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
  router
})
