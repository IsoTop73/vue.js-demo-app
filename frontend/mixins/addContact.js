export default {
  data() {
    return {
      error: null,
      message: null,
      contact: {}
    }
  },

  methods: {
    addContact() {
      fetch('api/add-contact', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(this.contact)
      })
      .then(res => {
        if (!res.ok) {
          this.error = true;
        }
        return res.json();
      })
      .then(res => {
        this.message = res.message;
        this.contact = {};
      })
      .catch(err => console.log(err));
    }
  }
}