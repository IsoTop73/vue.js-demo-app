export default {
  data() {
    return {
      error: null,
      message: null,
      deleted: false
    }
  },

  methods: {
    deleteContact() {
      fetch('/api/delete-contact', {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          id: this.contact._id
        })
      })
      .then(res => {
        if (!res.ok) {
          this.error = true;
        } else {
          this.deleted = true;
        }
        return res.json();
      })
      .then(res => {
        this.message = res.message;
      })
      .catch(err => console.log(err));
    }
  }
}