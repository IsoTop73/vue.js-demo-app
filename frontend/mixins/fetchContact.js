export default {
  data() {
    return {
      error: null,
      message: null,
      contact: null
    }
  },

  computed: {
    fullName() {
      return this.contact.firstName + ' ' + this.contact.lastName;
    },
    avatarSrc() {
      return 'img/contacts/' + this.contact.photo;
    }
  },

  methods: {
    fetchContact() {
      fetch('/api/contact/' + this.$route.params.id)
      .then(res => {
        if (!res.ok) {
          this.error = true;
        }
        return res.json();
      })
      .then(res => {
        if (this.error) {
          this.message = res.message;
        } else {
          this.contact = res;
        }
      })
      .catch(err => console.log(err));
    }
  }
}