export default {
  data() {
    return {
      error: null,
      message: null,
      contacts: null
    }
  },

  methods: {
    fetchContacts() {
      fetch('api/contacts.json')
      .then(res => {
        if (!res.ok) {
          this.error = true;
        }
        return res.json();
      })
      .then(res => {
        if (this.error) {
          this.message = res.message;
        } else {
          this.contacts = res.map(contact => {
            contact.fullName = contact.firstName + ' ' + contact.lastName;
            return contact;
          })
        }
      })
      .catch(err => console.log(err));
    }
  }
}