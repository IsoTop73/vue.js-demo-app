var path = require('path');
var webpack = require('webpack');
var merge = require('webpack-merge');
var autoprefixer = require('autoprefixer');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var isDevelopment;

switch(process.env.npm_lifecycle_event) {
  case 'build':
    process.env.NODE_ENV = 'production';
    isDevelopment = false;
    break;
  default:
    process.env.NODE_ENV = 'development';
    isDevelopment = true;
    break;
}

var baseConfig = {
  entry: {
    main: ['./frontend/main.js'],
    common: [
      './config/polyfills',
      'vue/dist/vue.js',
      'jquery',
      'bootstrap/dist/js/bootstrap.min.js'
    ]
  },

  output: {
    path: path.resolve('./backend/public'),
    filename: 'js/[name].[hash:8].js'
  },

  resolve: {
    extensions: ['', '.js', '.vue'],
    alias: {
      'vue$': 'vue/dist/vue.js'
    }
  },

  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue',
        exclude: /node_modules/
      },
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'file',
        query: {
          name: 'img/[name].[ext]'
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'file',
        query: {
          name: 'fonts/[name].[ext]'
        }
      }
    ]
  },

  plugins: [
    new webpack.ProvidePlugin({
      jQuery: 'jquery'
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
      minChunks: Infinity
    }),
    new ExtractTextPlugin('css/style.[chunkhash:8].css', {disable: isDevelopment}),
    new webpack.optimize.OccurrenceOrderPlugin()
  ],

  eslint: {
    formatter: require('eslint-friendly-formatter')
  },


  vue: {
    loaders: {
      css: ExtractTextPlugin.extract('vue-style-loader', 'css'),
      stylus: ExtractTextPlugin.extract('vue-style-loader', 'css!stylus')
    },
    
    postcss: [
      autoprefixer({
        browsers: [
          '>1%',
          'last 4 versions',
          'Firefox ESR',
          'not ie < 9'
        ]
      })
    ]
  }
};

isDevelopment
? module.exports = merge(baseConfig, require('./config/webpack.config.dev'))
: module.exports = merge(baseConfig, require('./config/webpack.config.prod'))